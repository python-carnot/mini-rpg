"use strict";

const matrice = [
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 4, 4, 0, 0, 0, 0, 0],
  [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0],
  [0, 0, 1, 2, 2, 2, 2, 2, 2, 1, 0, 0],
  [0, 0, 1, 2, 3, 3, 3, 3, 2, 1, 0, 0],
  [0, 0, 1, 2, 3, 3, 3, 3, 2, 1, 0, 0],
  [0, 0, 1, 2, 2, 2, 2, 2, 2, 1, 0, 0],
  [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
];

const paint = {
  0: 'waterBlock',
  1: 'grassBlock',
  2: 'stoneBlock',
  3: 'dirtBlock',
  4: 'woodBlock',
};

function afficheTuile(nomBrique, i, j, dx=0, dy=0) {
    g.ctx.drawImage(g.images[nomBrique], i*50 + dx, -25+j*40 + dy, 50, 85);
}

function affichage() {
    afficheTerrain();
    affichePerso();
    
    déplacePerso();
    
    // On rappelle cette même fonction dès que possible:
    window.requestAnimationFrame(affichage)
}

function afficheTerrain() {
    // Temps en secondes
    const t = Date.now()*0.001;
    const amplitude = 3;
    const vitesse = 5;
    
    for (let i = 0; i < 12; i++) {
        for (let j = 0; j < 10; j++) {
            const numTuile = matrice[j][i];
            const nomTuile = paint[numTuile];
            
            let dy = 0;
            if (nomTuile == 'waterBlock') {
                const angle = vitesse*t + i + j;
                dy = amplitude*Math.sin(angle);
            }
            afficheTuile(nomTuile, i, j, 0, dy);
        }
    }
}

function affichePerso() {
    // Temps en secondes
    const t = Date.now()*0.001;
    const amplitude = 1;
    const vitesse = 10;

    const angle = vitesse*t;
    const dy = Math.floor(amplitude*Math.sin(angle));

    afficheTuile(g.persoTuile, g.persoX, g.persoY, g.persoDX - 25, g.persoDY - 38 + dy);
}

function touchePressée(event) {
    const touche = event.key;
    
    if (touche == 'ArrowDown') {
        g.toucheFlècheBas = true;
    }
    if (touche == 'ArrowUp') {
        g.toucheFlècheHaut = true;
    }
    if (touche == 'ArrowLeft') {
        g.toucheFlècheGauche = true;
    }
    if (touche == 'ArrowRight') {
        g.toucheFlècheDroite = true;
    }
}

function toucheRelachée(event) {
    const touche = event.key;
    
    if (touche == 'ArrowDown') {
        g.toucheFlècheBas = false;
    }
    if (touche == 'ArrowUp') {
        g.toucheFlècheHaut = false;
    }
    if (touche == 'ArrowLeft') {
        g.toucheFlècheGauche = false;
    }
    if (touche == 'ArrowRight') {
        g.toucheFlècheDroite = false;
    }
}

function déplacePerso() {
    if (g.ancienTemps != undefined) {
        // Vitesse de déplacement souhaitée, 
        // en pixels par seconde
        const vitesse = 300;
        
        // On a besoin de connaître précisément combien de
        // secondes (ou fraction de seconde) se sont
        // écoulées depuis l'affichage précédent
        //
        // Attention, Date.now() est en millisecondes.
        //
        const dt = (Date.now() - g.ancienTemps) * 0.001;
        
        // On calcule le décallage effectif à appliquer
        // Pour respecter la vitesse demandée
        const déplacement = dt * vitesse;
        //console.log(dt, déplacement);

        let px = g.persoX;
        let py = g.persoY;
        let dx = g.persoDX;
        let dy = g.persoDY;
        
        if (g.toucheFlècheBas) {
            dy += déplacement;
        }
        if (g.toucheFlècheHaut) {
            dy -= déplacement;
        }
        if (g.toucheFlècheDroite) {
            dx += déplacement;
        }
        if (g.toucheFlècheGauche) {
            dx -= déplacement;
        }
        
        // A-t-on bougé ?
        if (dx != g.persoDX || dy != g.persoDY) {
            if (dx < 0) {
                px--;
                dx += 50;
            } else if (dx >= 50) {
                px++;
                dx -= 50;
            }
            
            if (dy < 0) {
                py--;
                dy += 40;
            } else if (dy >= 40) {
                py++;
                dy -= 40;
            }
            
            // Le déplacement est-il valide ?
            if (paint[matrice[py][px]] != 'waterBlock') {
                // On valide le déplacement
                console.log(g.persoX, g.persoY, dx, dy);
                g.persoX = px;
                g.persoY = py;
                g.persoDX = dx;
                g.persoDY = dy;
            }
        }
    }
    
    // On garde en mémoire l'heure actuelle, afin de 
    // pouvoir calculer dt au prochain affichage:
    g.ancienTemps = Date.now();
}


