"use strict";
var g = {
    images: {},
    imagesChargées: {},
}

const nomsImages = {
    characterBoy: 'images/Character Boy.png',
    characterCatGirl: 'images/Character Cat Girl.png',
    characterHornGirl: 'images/Character Horn Girl.png',
    characterPinkGirl: 'images/Character Pink Girl.png',
    characterPrincessGirl: 'images/Character Princess Girl.png',
    chestClosed: 'images/Chest Closed.png',
    chestLid: 'images/Chest Lid.png',
    chestOpen: 'images/Chest Open.png',
    dirtBlock: 'images/Dirt Block.png',
    doorTallClosed: 'images/Door Tall Closed.png',
    doorTallOpen: 'images/Door Tall Open.png',
    enemyBug: 'images/Enemy Bug.png',
    gemBlue: 'images/Gem Blue.png',
    gemGreen: 'images/Gem Green.png',
    gemOrange: 'images/Gem Orange.png',
    grassBlock: 'images/Grass Block.png',
    heart: 'images/Heart.png',
    key: 'images/Key.png',
    plainBlock: 'images/Plain Block.png',
    rampEast: 'images/Ramp East.png',
    rampNorth: 'images/Ramp North.png',
    rampSouth: 'images/Ramp South.png',
    rampWest: 'images/Ramp West.png',
    rock: 'images/Rock.png',
    roofEast: 'images/Roof East.png',
    roofNorth: 'images/Roof North.png',
    roofNorthEast: 'images/Roof North East.png',
    roofNorthWest: 'images/Roof North West.png',
    roofSouth: 'images/Roof South.png',
    roofSouthEast: 'images/Roof South East.png',
    roofSouthWest: 'images/Roof South West.png',
    roofWest: 'images/Roof West.png',
    selector: 'images/Selector.png',
    shadowEast: 'images/Shadow East.png',
    shadowNorth: 'images/Shadow North.png',
    shadowNorthEast: 'images/Shadow North East.png',
    shadowNorthWest: 'images/Shadow North West.png',
    shadowSideWest: 'images/Shadow Side West.png',
    shadowSouth: 'images/Shadow South.png',
    shadowSouthEast: 'images/Shadow South East.png',
    shadowSouthWest: 'images/Shadow South West.png',
    shadowWest: 'images/Shadow West.png',
    speechbubble: 'images/SpeechBubble.png',
    star: 'images/Star.png',
    stoneBlock: 'images/Stone Block.png',
    stoneBlockTall: 'images/Stone Block Tall.png',
    treeShort: 'images/Tree Short.png',
    treeTall: 'images/Tree Tall.png',
    treeUgly: 'images/Tree Ugly.png',
    wallBlock: 'images/Wall Block.png',
    wallBlockTall: 'images/Wall Block Tall.png',
    waterBlock: 'images/Water Block.png',
    windowTall: 'images/Window Tall.png',
    woodBlock: 'images/Wood Block.png',
}

function chargement(event) {
    g.imagesChargées[event.target.src] = true;
    //console.log("Une image vient d'être chargée", event.target.src);
    //console.log(g.imagesChargées)
}   

function toutesLesImagesSontChargées() {
    // Renvoie true/false selon que toutes les
    // images ont été chargées (ou non)
    
    let compteur = 0;
    Object.keys(g.imagesChargées).forEach((clé) => {
        if (!g.imagesChargées[clé]) {
            //console.log(`${compteur} images ont été chargées`);
            // On a trouvé une image pas encore chargée
            return false;
        }
        compteur ++;
    });
    
    // Toutes les images ont été chargées
    return true;
}

function testeImageChargée() {
    if (toutesLesImagesSontChargées()) {
        //console.log("Les images sont chargées");   
        clearInterval(g.idTesteImageChargée);
        // On appelle la fonction d'affichage environ 60x par secondes:
        // g.idAnimation = setInterval(afficheTerrain, 16)
        // 
        // alternative:
        //
        // On appelle la fonction d'affichage dès que possible
        //
        window.requestAnimationFrame(affichage)
        
    } else {
        //console.log("Image pas encore chargée...");
    }
}

function chargeToutesLesImages() {
    // On lance le chargement de toutes les images
    Object.keys(nomsImages).forEach((nom) => {
        const image = new Image();
        image.src = nomsImages[nom];
        g.images[nom] = image;
        g.imagesChargées[nomsImages[nom]] = false;
    });
    
    // On teste que toutes les images ont bien été chargées
    Object.keys(nomsImages).forEach((nom) => {
        g.images[nom].addEventListener("load", chargement);
    });
    g.idTesteImageChargée = setInterval(testeImageChargée, 16);
}

function initialisation() {
    g.canvas = document.querySelector("#canevas");
    g.ctx = g.canvas.getContext("2d");

    g.persoX = 5;
    g.persoY = 1;
    g.persoDX = 15;
    g.persoDY = 18;
    g.persoTuile = 'characterHornGirl';

    g.largeurTerrain = 50;
    g.hauteurTerrain = 40;

    g.toucheFlècheGauche = false;
    g.toucheFlècheDroite = false;
    g.toucheFlècheHaut = false;
    g.toucheFlècheBas = false;

    window.addEventListener('keydown', touchePressée);
    window.addEventListener('keyup', toucheRelachée);

    chargeToutesLesImages();    
}


