# Mini RPG

Un petit RPG sans prétention, utilisant le tileset libre "planet cute".

# Tileset "Planet Cute"

Ce [tileset](https://fr.wikipedia.org/wiki/Tuile_(jeu_vid%C3%A9o)) est [mis à disposition](https://lostgarden.home.blog/2007/05/12/dancs-miraculously-flexible-game-prototyping-tiles/#) gratuitement et librement par Daniel "Danc" Cook sur son [blog](https://lostgarden.home.blog/). 



